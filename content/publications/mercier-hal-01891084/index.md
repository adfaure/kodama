+++
title = "Considering the Development Workflow to Achieve Reproducibility with Variation"
date = 2018-11-01

[extra]
type = "Conference"
authors = ["Michael Mercier", "Adrien Faure", "Olivier Richard"]
publication_types = "Conference paper"
featured = true
publication = "SC 2018 - Workshop: ResCuE-HPC"
tags = ["nix", "reproducibility"]
url_pdf = "https://hal.inria.fr/hal-01891084/file/ws_rescue102s1-file1.pdf"
+++