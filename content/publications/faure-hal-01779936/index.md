+++
title = "Évaluation d'algorithmes d'ordonnancement par simulation réaliste"
date = 2018-04-01

[extra]
type = "Conference"
authors = ["Adrien Faure", "Millian Poquet", "Olivier Richard"]
publication = "Compass"
publication_types = "Preprint"

url_pdf = "https://hal.inria.fr/hal-01779936/file/paper.pdf"
url_slides = "files/slides_compas18.pdf"
+++

