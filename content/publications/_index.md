+++
title = "Publications"
weight = 3
sort_by = "date"
template = "publications.html"
page_template = "publication-page.html"
insert_anchor_links = "right"

extra.index_title = "Recent publications"
extra.name = "Research"
extra.index_show = true
+++

## Topic of interest

My current research activities focus on job management on scheduling for HPC
systems.

- Resources and Job Management System
- Parallel Job Scheduling
- Simulation and Modelisation
- Reproducibility
