+++
title = "Adrien Faure"
date = 2020-07-09
sort_by = "date"
template = "publications.html"
page_template = "publication-page.html"
insert_anchor_links = "right"
extra.name = "Research"
+++

## Topic of interest

My current research activities focus on job management on scheduling for HPC
systems.

- Resources and Job Management System
- Parallel Job Scheduling
- Simulation and Modelisation
- Reproducibility
