+++
title = "Advanced Simulation for Resource Management"
date = 2020-12-02

[extra]
type = "These"
publication_types = "Thesis"
url_pdf = "publications/these/thesis_afaure.pdf"
url_slides = "publications/these/thesis_slides_afaure.pdf"
+++
